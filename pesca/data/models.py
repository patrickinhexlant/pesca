import django
from django.db import models
from django.contrib.auth.models import User
from enum import Enum


class UserInfo(models.Model):
    user = models.OneToOneField(django.contrib.auth.models.User, on_delete=models.CASCADE)
    user_type = models.SmallIntegerField(default=0)
    nickname = models.CharField(max_length=128, default="")
    profile_picture = models.FileField(upload_to='media/%Y/%m/%d/', null=True, blank=True)

    class Type(Enum):
        HOMEPAGE=0

    def __str__(self):
        return str(self.user);


class Game(models.Model):
    name = models.CharField(max_length=128, default="")
    desc = models.TextField(default="")
    thumbnail = models.FileField(upload_to='%Y/%m/%d/', null=True, blank=True)

    def __str__(self):
        return self.name;


class GameCategory(models.Model):
    name = models.CharField(max_length=128, default="")
    desc = models.TextField(default="")

    def __str__(self):
        return self.name;


class GameUser(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    user = models.ForeignKey(django.contrib.auth.models.User, on_delete=models.CASCADE)
    data = models.TextField(default="{}")

    class Meta:
        unique_together = (("game", "user"),)

    def __str__(self):
        return str(self.game) + "-" + str(self.user)


class GameCategoryGame(models.Model):
    game_category = models.ForeignKey(GameCategory, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("game_category", "game"),)

    def __str__(self):
        return str(self.game_category) + "-" + str(self.game)