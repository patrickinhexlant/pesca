from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.db.models import Q
from .models import *
from .serializers import *
from rest_framework import generics
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import *


class UserMyInfoView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Game.objects.all()
    serializer_class = UserInfoSerializer

    def get(self, request, format=None):
        content = {
            'user': str(request.user),  # `django.contrib.auth.User` instance.
            'auth': str(request.auth),  # None
        }
        return Response(content)


@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def user_my_info(request):
    serializer = UserInfoSerializer(UserInfo.objects.get(user=request.user))
    return Response(serializer.data)


@api_view(['POST'])
def user_login(request):
    username = request.POST["username"]
    password = request.POST["password"]

    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        user_info = UserInfo.objects.get(user=user)
        serializer = UserInfoSerializer(user_info)
        return Response(serializer.data)

    return HttpResponse('Unauthorized', status=401)

@api_view(['POST'])
def user_create(request):
    username = request.POST["username"]
    password = request.POST["password"]
    nickname = request.POST["nickname"]
    user = User.objects.create_user(username=username,
                                    password=password)
    user_info = UserInfo.objects.create(user=user, nickname=nickname)
    serializer = UserInfoSerializer(user_info)

    login(request, user)

    return Response(serializer.data)


class GameView(generics.RetrieveAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class GameListView(generics.ListAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Game.objects.all()
        game_category_id = self.request.query_params.get('game_category_id', None)
        if game_category_id is not None:
            queryset = Game.objects.filter(id__in=GameCategoryGame.objects.filter(game_category_id=game_category_id).values("game"))
        return queryset


class GameCategoryListView(generics.ListAPIView):
    queryset = GameCategory.objects.all()
    serializer_class = GameCategorySerializer


class GameCategoryView(generics.RetrieveAPIView):
    queryset = GameCategory.objects.all()
    serializer_class = GameSerializer