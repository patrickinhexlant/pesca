from django.urls import include, path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('api/auth/', include('rest_auth.urls')),
    url(r'^api/user/login$', views.user_login),
    url(r'^api/user/create$', views.user_create),
    url(r'^api/user/my_info$', views.user_my_info),
    url(r'^api/game/(?P<pk>[0-9]+)$', views.GameView.as_view()),
    url(r'^api/game/list$', views.GameListView.as_view()),
    url(r'^api/game_category/list$', views.GameCategoryListView.as_view()),
    url(r'^api/game_category/(?P<pk>[0-9]+)', views.GameCategoryView.as_view()),
]