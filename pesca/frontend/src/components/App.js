import React from "react";
import ReactDOM from "react-dom";
import DataProvider from "./DataProvider";
import GameCategoryListWithGame from "./GameCategoryListWithGame";
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import {withRouter} from 'react-router-dom';
import CSRFToken from "./CSRFToken"

const LoginContext = React.createContext();

class LoginContextProvider extends React.Component {
    state = {
        userLoggedIn: false,
        userNickName: "",
        userProfilePicture: null,
        logout: () => {
            let csrftoken = getCookie('csrftoken');
            fetch("/api/auth/logout/", {
                credentials: 'include',
                method: 'POST',
                mode: 'same-origin',
                headers: {
                    'Accept': 'application/json',
                    'X-CSRFToken': csrftoken,
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-Requested-With': 'XMLHttpRequest'
                },
            }).then(response => {
                    if (response.status !== 200) {
                        // TODO
                        return;
                    }
                    this.setState({
                        userLoggedIn: false,
                        userNickname: "",
                        userProfilePicture: ""
                    });
                    return response.json();
            })
        },
        login: (data) => {
            this.setState({
                userLoggedIn: true,
                userNickname: data.nickname,
                userProfilePicture: data.profile_picture
            });
        }
    }

    componentDidMount() {
        fetch("/api/user/my_info", {
            credentials: 'include'
        })
            .then(response => {
                if (response.status !== 200) {
                    this.setState({
                        userLoggedIn: false,
                        userNickName: "",
                        userProfilePicture: null,
                    })
                    return;
                }
                return response.json();
            }).then(data => data && this.state.login(data));
    }

    render() {
        return <LoginContext.Provider value={this.state}>
                    {this.props.children}
                </LoginContext.Provider>
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogout = (context) => {
            context.logout();
            this.props.history.push("/")
        }
    }

    render() {
        return <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/casino_game">Casino Game</Link>
                        </li>
                        <li>
                            <Link to="/live_casino">Live Casino</Link>
                        </li>
                        <li>
                            <Link to="/support">Support</Link>
                        </li>
                    </ul>
                    <div className="pull-right">
                        <ul className="nav navbar-nav">
                            <LoginContext.Consumer>
                                {(context) => {
                                    let ret;
                                    if (context.userLoggedIn) {
                                        ret = <React.Fragment>
                                            <li>
                                                <a>
                                                    Hi! {context.userNickname}
                                                </a>
                                            </li>
                                            <li>
                                                <a onClick={this.handleLogout.bind(this, context)}> Logout </a>
                                            </li>
                                        </React.Fragment>
                                    } else {
                                        ret = <React.Fragment>
                                            <li><Link to="/login">
                                                Login
                                            </Link></li>
                                            <li>
                                                <Link to="/join">
                                                    Join
                                                </Link>
                                            </li>
                                        </React.Fragment>
                                    }
                                    return ret;
                                }}
                            </LoginContext.Consumer>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    }
}

class Join extends React.Component {
    constructor(props) {
        super(props);
        this.state = {username: "", password: "", nickname: "", placeholder: ""};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this);
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this);
        this.handleNicknameChanged = this.handleNicknameChanged.bind(this);
    }

    handleUsernameChanged(event) {
        this.setState({username: event.target.value});
    }

    handlePasswordChanged(event) {
        this.setState({password: event.target.value});
    }

    handleNicknameChanged(event) {
        this.setState({nickname: event.target.value});
    }

    handleSubmit(event) {
        let csrftoken = getCookie('csrftoken');
        let formData = new FormData();
        let body = {
            "username": this.state.username,
            "password": this.state.password,
            "nickname": this.state.nickname,
        };
        fetch("/api/user/create", {
            credentials: 'include',
            method: 'POST',
            mode: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: xwwwfurlenc(body)
        }).then(response => {
            if (response.status !== 200) {
                this.setState({placeholder: "Something went wrong"});
            }
            return response.json();
        }).then(data => {
            this.props.loginContext.login(data)
            this.props.history.push('/')
        });
        event.preventDefault();
    }

    render() {
        return <form onSubmit={this.handleSubmit}>
                    <CSRFToken/>
                    <p>
                        {this.state.placeholder}
                    </p>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" placeholder="Username"
                               required="true" onChange={this.handleUsernameChanged}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" placeholder="Password"
                               required="true" onChange={this.handlePasswordChanged}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="nickname">Nickname</label>
                        <input type="text" className="form-control" name="nickname" placeholder="Nickname"
                               onChange={this.handleNicknameChanged}/>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
    }
}

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {username: "", password: ""};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this);
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this);
    }

    handleUsernameChanged(event) {
        this.setState({username: event.target.value});
    }

    handlePasswordChanged(event) {
        this.setState({password: event.target.value});
    }

    handleSubmit(event) {
        let csrftoken = getCookie('csrftoken');
        let formData = new FormData();
        let body = {
            "username": this.state.username,
            "password": this.state.password,
        };
        fetch("/api/user/login", {
            credentials: 'include',
            method: 'POST',
            mode: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: xwwwfurlenc(body)
        }).then(response => {
            if (response.status !== 200) {
                this.setState({placeholder: "Something went wrong"});
                return null;
            } else {
            }
            return response.json();
        }).then(data => {
            if (data) {
                let loginContext = this.props.loginContext;
                loginContext.login(data);
                this.props.history.push('/')
            }
        });
        event.preventDefault();
    }

    render() {
        return <form onSubmit={this.handleSubmit}>
                    <CSRFToken/>
                    <p>
                        {this.state.placeholder}
                    </p>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" placeholder="Username"
                               required="true" onChange={this.handleUsernameChanged}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" placeholder="Password"
                               required="true" onChange={this.handlePasswordChanged}/>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
    }
}

const Home = () => (
    <DataProvider endpoint="/api/game_category/list"
                  render={data => <GameCategoryListWithGame data={data.results}/>}/>
);

const CasinoGame = () => (
    <DataProvider endpoint="/api/game_category/list"
              render={data => <GameCategoryListWithGame data={data.results}/>}/>
);

const LiveCasino = () => (
    <DataProvider endpoint="/api/game_category/list"
                  render={data => <GameCategoryListWithGame data={data.results}/>}/>
);

const Support = () => (
    <div>
        Support
    </div>
);

const HeaderWithRouter = withRouter(Header)

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <LoginContextProvider>
                    <Router>
                        <div>
                            <HeaderWithRouter {...this.props} />
                                <div>
                                    <div className="container" style={{paddingTop: "5em"}}>
                                        <Route exact path="/" component={Home}/>
                                        <Route path="/casino_game" component={CasinoGame}/>
                                        <Route path="/live_casino" component={LiveCasino}/>
                                        <Route path="/join"
                                               render={(props) => {
                                                   return <LoginContext.Consumer>
                                                                {context => <Join {...props} loginContext={context} /> }
                                                            </LoginContext.Consumer>
                                               }}
                                        />
                                        <Route path="/login" render={(props) => {
                                                   return <LoginContext.Consumer>
                                                                {context => <Login {...props} loginContext={context} /> }
                                                            </LoginContext.Consumer>
                                               }}/>
                                    </div>
                                </div>
                        </div>
                    </Router>
                </LoginContextProvider>
    }
}

export default App

const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(
    <App/>
    , wrapper) : null;