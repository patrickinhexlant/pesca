from rest_framework import serializers
from data.models import *


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfo
        fields = '__all__'


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = '__all__'


class GameCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GameCategory
        fields = '__all__'


class GameCategoryGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameCategoryGame
        fields = '__all__'
