import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";
import Game from "./Game";
import GameCategory from "./GameCategory";
import DataProvider from "./DataProvider";

const GameCategoryListWithGame = ({data}) =>
    !data.length ? (
        <p>Loading..</p>
    ) : (
        <div>
            <div className="row">
                <div className="col-xs-12">
                    {data.map(el => (
                        <React.Fragment key={key(el)} >
                            <DataProvider endpoint={"/api/game_category/" + el.id} render={ data => <GameCategory key={key(data)} data={data}/> }/>
                            <DataProvider endpoint={"/api/game/list?game_category_id=" + el.id} render={ data =>
                                data.results.map(el => (
                                    <Game key={key(el)} data={el}/>
                                ))
                            }/>
                        </React.Fragment>
                    ))}
                </div>
            </div>
        </div>
    );
GameCategoryListWithGame.propTypes = {
  data: PropTypes.array.isRequired
};
export default GameCategoryListWithGame;