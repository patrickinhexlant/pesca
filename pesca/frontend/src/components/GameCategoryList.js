import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";
import GameCategory from "./GameCategory";
const GameCategoryList = ({ data }) =>
  !data.length ? (
    <p>Loading..</p>
  ) : (
    <div className="row">
        <div className="col-xs-12">
            {data.map(el => (
                <GameCategory key={key(el)} data={el}/>
            ))}
        </div>
    </div>
  );
GameCategoryList.propTypes = {
  data: PropTypes.array.isRequired
};
export default GameCategoryList;