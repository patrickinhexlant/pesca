from django.contrib import admin
from .models import *

admin.site.register(UserInfo)
admin.site.register(Game)
admin.site.register(GameCategory)
admin.site.register(GameCategoryGame)
admin.site.register(GameUser)
