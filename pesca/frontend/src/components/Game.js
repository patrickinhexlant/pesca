import React from "react";
import PropTypes from "prop-types";
const Game = ({ data }) =>
  (
      <div className="row">
        <div className="col-xs-12">
            <div style={{border: "black 1px solid"}}>
                <p>{data.name}</p>
                {data.thumbnail ? (<img src={data.thumbnail}/>) : "" }<br/>
                {data.desc}
            </div>
        </div>
      </div>
  );
Game.propTypes = {
  data: PropTypes.object.isRequired
};
export default Game;