import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";
const GameCategory = ({ data }) =>
  (
      <div className="row">
        <div className="col-xs-12">
            {data.name} - {data.desc} - {data.thumbnail}
        </div>
      </div>
  );
GameCategory.propTypes = {
  data: PropTypes.object.isRequired
};
export default GameCategory;